﻿using Mas_Global.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Mas_Global.Web.Controllers
{
    public class EmployeesController : Controller
    {
        private string employeServices = "api/Employees";
        // GET: Employees
        public ActionResult Index()
        {
            var response = callAPI(employeServices);
            var result = JsonConvert.DeserializeObject<List<Employee>>(response);

            return View("Index", result);
        }

        // GET: Employees/Details/5
        public ActionResult Details(string id)
        {
            if (id == string.Empty)
                return Index();
            
            var response = callAPI(employeServices + "/" + id);
            if (response != null)
            {
                List<Employee> result = new List<Employee>();
                result.Add(JsonConvert.DeserializeObject<Employee>(response));
                return View("Index", result);
            }
            else
            {
                ViewBag.Message = "The Employee #" + id + " is not exist.";
                return Index();
            }
            
        }

        private string callAPI(string services)
        {
            string textRemote = string.Empty;
            string URL = ConfigurationManager.AppSettings["baseServices"] + services;
            try
            { 
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Method = "GET";
                request.ContentType = "application/text; charset=utf-8";
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if ((response.StatusCode == HttpStatusCode.Accepted) ||
                        (response.StatusCode == HttpStatusCode.Created) ||
                        (response.StatusCode == HttpStatusCode.OK) ||
                        (response.StatusCode == HttpStatusCode.PartialContent))
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                            textRemote = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError &&
                    ex.Response != null)
                {
                    var resp = (HttpWebResponse)ex.Response;
                    if (resp.StatusCode == HttpStatusCode.NotFound)
                    {
                        return null;
                    }
                    else
                    {
                        // Do something else
                    }
                }
                else
                {
                    // Do something else
                }
            }
            return textRemote;
        }
    }
}
