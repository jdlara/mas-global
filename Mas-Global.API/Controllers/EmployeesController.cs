﻿using Mas_Global.Entities;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Mas_Global.API.Controllers
{
    public class EmployeesController : ApiController
    {
        [Route("api/Employees")]
        public HttpResponseMessage Get()
        {
            BusinessLibrary.Employees bl = new BusinessLibrary.Employees();
            var result = bl.GetAllEmployees();
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("api/Employees/{id}")]
        public HttpResponseMessage Get(int id)
        {
            BusinessLibrary.Employees bl = new BusinessLibrary.Employees();
            var employee = bl.GetById(id);

            if(employee != null)
                return Request.CreateResponse(HttpStatusCode.OK, employee); 
            else
                return Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}
