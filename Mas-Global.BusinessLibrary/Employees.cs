﻿using Mas_Global.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mas_Global.BusinessLibrary
{
    public class Employees
    {
        public List<Employee> GetAllEmployees()
        {
            Proxy.Employees pr = new Proxy.Employees();
            return pr.GetAllEmployees();
        }

        public Employee GetById(int id)
        {
            Proxy.Employees pr = new Proxy.Employees();
            var result = pr.GetAllEmployees();

            Employee employee = (from emp in result
                                 where emp.id == id
                                 select emp).FirstOrDefault() as Employee;

            return employee;
        }
    }
}
