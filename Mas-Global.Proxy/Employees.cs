﻿using Mas_Global.Entities;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Configuration;

namespace Mas_Global.Proxy
{
    public class Employees
    {
        private string employeServices = "api/Employees";
        public List<Employee> GetAllEmployees()
        {
            List<Employee> result = null;
            try
            {
                string url = ConfigurationManager.AppSettings["baseServices"] + employeServices;
                var client = new WebClient();
                var response = client.DownloadString(url);
                result = JsonConvert.DeserializeObject<List<Employee>>(response);
            }
            catch (Exception ex)
            {
                ex = new Exception();
            }
            return result;
        }
    }
}
