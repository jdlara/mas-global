﻿using Mas_Global.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace Mas_Global.Tests
{
    [TestClass]
    public class TestSource
    {
        private string employeServices = "api/Employees";
        [TestMethod]
        public void TestAzureSource()
        {
            List<Employee> result = null;
            try
            {
                string url = ConfigurationManager.AppSettings["baseServices"] + employeServices;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.ContentType = "application/json; charset=utf-8";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if ((response.StatusCode == HttpStatusCode.Accepted) ||
                        (response.StatusCode == HttpStatusCode.Created) ||
                        (response.StatusCode == HttpStatusCode.OK) ||
                        (response.StatusCode == HttpStatusCode.PartialContent))
                    {
                        string textRemote = string.Empty;
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                            textRemote = reader.ReadToEnd();
                        }
                        result = JsonConvert.DeserializeObject<List<Employee>>(textRemote);
                    }
                }
            }catch(Exception ex)
            {
                Assert.Fail();
            }
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        public void TestGetAll()
        {
            BusinessLibrary.Employees emp = new BusinessLibrary.Employees();
            var listOfEmployees = emp.GetAllEmployees();
            Assert.AreEqual(2, listOfEmployees.Count);
        }

        [TestMethod]
        public void TestGetByIdNotExist()
        {
            BusinessLibrary.Employees emp = new BusinessLibrary.Employees();
            var employee = emp.GetById(5);
            Assert.AreEqual(null, employee);
        }

        [TestMethod]
        public void TestGetMonthlySalary()
        {
            BusinessLibrary.Employees emp = new BusinessLibrary.Employees();
            var employee = emp.GetById(1);
            Assert.AreEqual(86400000, employee.annualSalary);
        }

        [TestMethod]
        public void TestGetHourlySalary()
        {
            BusinessLibrary.Employees emp = new BusinessLibrary.Employees();
            var employee = emp.GetById(2);
            Assert.AreEqual(960000, employee.annualSalary);
        }
    }
}
