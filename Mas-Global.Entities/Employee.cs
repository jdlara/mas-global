﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mas_Global.Entities
{
    public class Employee: EmployeeBase
    {

        public double annualSalary
        {
            get
            {
                double Salary;
                if (contractTypeName == "HourlySalaryEmployee")
                    Salary = (120 * hourlySalary * 12);
                else
                    Salary = (monthlySalary * 12);

                return Salary;
            }
        }
    }
}
